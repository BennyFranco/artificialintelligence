﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowWaypoint : MonoBehaviour
{
    public GameObject[] waypoints;
    public float speed = 10.0f;
    public float rotSpeed = 10.0f;
    public float lookAhead = 10.0f;

    private int currentWaypoint = 0;
    private GameObject tracker;

    // Start is called before the first frame update
    void Start()
    {
        tracker = GameObject.CreatePrimitive(PrimitiveType.Capsule);
        Destroy(tracker.GetComponent<Collider>());
        tracker.GetComponent<MeshRenderer>().enabled = false;
        tracker.transform.position = transform.position;
        tracker.transform.rotation = transform.rotation;
    }

    void ProgressTracker()
    {
        if (Vector3.Distance(tracker.transform.position, transform.position) > lookAhead)
            return;

        if (Vector3.Distance(tracker.transform.position, waypoints[currentWaypoint].transform.position) < 3)
        {
            currentWaypoint++;
        }

        if (currentWaypoint >= waypoints.Length)
            currentWaypoint = 0;

        tracker.transform.LookAt(waypoints[currentWaypoint].transform);
        tracker.transform.Translate(0, 0, (speed+2)*Time.deltaTime);
    }

    // Update is called once per frame
    void Update()
    {
        ProgressTracker();
        //    if (Vector3.Distance(transform.position, waypoints[currentWaypoint].transform.position) < 10)
        //    {
        //        currentWaypoint++;
        //    }

        //    if (currentWaypoint >= waypoints.Length)
        //        currentWaypoint = 0;

        //    // transform.LookAt(waypoints[currentWaypoint].transform);

        Quaternion lookAtWaypoint = Quaternion.LookRotation(tracker.transform.position - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, lookAtWaypoint, rotSpeed * Time.deltaTime);

        transform.Translate(0, 0, speed * Time.deltaTime);
    }
}
