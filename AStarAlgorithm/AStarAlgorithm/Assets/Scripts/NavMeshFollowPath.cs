﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavMeshFollowPath : MonoBehaviour
{
    public GameObject wpManager;
    GameObject[] wps;

    UnityEngine.AI.NavMeshAgent agent;

    // Start is called before the first frame update
    void Start()
    {
        wps = wpManager.GetComponent<WPManager>().waypoints;
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
    }

    public void GoToHeli()
    {
        agent.SetDestination(wps[1].transform.position);
    }

    public void GoToRuin()
    {
        agent.SetDestination(wps[4].transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
